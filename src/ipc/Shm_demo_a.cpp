//
// Created by zhaoyf on 2022/5/4.
//
#include <thread>
#include <chrono>

#include <stdio.h>
#include <string.h>

#include <sys/ipc.h>
#include <sys/shm.h>


#define BUF_SIZE 1024
#define SHM_KEY 0x19

#ifdef PROJECT_BUILD
int shmdemoa() {
#else

int main() {
#endif
    int shmid;
    void *shmWritePtr = NULL;

    if ((shmid = shmget(SHM_KEY, BUF_SIZE, IPC_CREAT)) == -1) {
        printf("shmget error \n");
        return 1;
    }
    shmWritePtr = shmat(shmid, NULL, 0);
    if (shmWritePtr == NULL) {
        printf("shmat error!\n");
        return 1;
    }

    while (1) {
        printf("input: ");
        scanf("%s", shmWritePtr);
        std::this_thread::sleep_for(std::chrono::milliseconds(5000));
    }
    return 0;
}

