//
// Created by zhaoyf on 2022/5/3.
//

#include "Semaphore.h"

#include <stdio.h>

#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>

namespace Semaphore {
    /* 定义自己的semun联合体*/
    union semun {
        int val;
        struct semid_ds *buf;
        unsigned short *array;
        struct seminfo *__buf;
    };

    /// 创建或者获取已存在的信号量
    /// \param num 要创建的信号量集中信号量数量， [0, 1, 2, ..., n-1]
    /// \param key 创建信号量的 key
    /// \return    信号量id
    int SemInit(int num, int key) {
        int semid = -1;
        semid = semget((key_t) key, num, IPC_CREAT | IPC_EXCL | 0600);
        if (semid == -1) {
            // 指定 key 信号量已存在
            semid = semget((key_t) 1234, 1, 0600);
        } else {
            // 设置初始值
            union semun a;
            a.val = 1;//信号量的初始值
            if (semctl(semid, 0, SETVAL, a) == -1) {
                perror("semctl error");
            }
        }
        return semid;
    }

    /// p操作，信号量值减1
    /// \param semid 信号量id
    void SemP(int semid) {
        struct sembuf buf;
        buf.sem_num = 0;
        buf.sem_op = -1;//p
        buf.sem_flg = SEM_UNDO;
        if (semop(semid, &buf, 1) == -1) {
            perror("semop p error");
        }
    }

    /// v操作，信号量值加1
    /// \param semid 信号量id
    void SemV(int semid) {
        struct sembuf buf;
        buf.sem_num = 0;
        buf.sem_op = 1;//v
        buf.sem_flg = SEM_UNDO;
        if (semop(semid, &buf, 1) == -1) {
            perror("semop v error");
        }
    }

    /// 销毁信号量集中的所有信号量
    /// \param semid 信号量id
    /// \param num   信号量数量
    void SemRelease(int semid, int num) {
        for (int i = 0; i < num; ++i) {
            if (semctl(semid, i, IPC_RMID) == -1) {
                perror("semctl del error");
            }
        }
    }
}