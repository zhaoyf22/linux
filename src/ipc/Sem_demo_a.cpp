//
// Created by zhaoyf on 2022/5/4.
//
#include "Semaphore.h"

#include <thread>
#include <chrono>

#include <stdio.h>

// 进程1代码
#ifdef PROJECT_BUILD
int semdemoa() {
#else

int main() {
#endif
    int semId = Semaphore::SemInit(1, 1234);
    const int NUM = 5;
    for (int i = 0; i < NUM; ++i) {
        Semaphore::SemP(semId);
        printf("a");
        fflush(stdout);
        int n = rand() % 3000;
        std::this_thread::sleep_for(std::chrono::milliseconds(n));
        printf("a");
        fflush(stdout);
        Semaphore::SemV(semId);

        n = rand() % 3000;
        std::this_thread::sleep_for(std::chrono::milliseconds(n));
    }
//    Semaphore::SemRelease(semId, 1);
    return 0;
}
