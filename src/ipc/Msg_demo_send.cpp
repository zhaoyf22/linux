//
// Created by zhaoyf on 2022/5/4.
//

#include <thread>
#include <chrono>

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>

#include <sys/msg.h>


#define MAX_TEXT 512
struct msg_st {
    long int msg_type;
    char text[MAX_TEXT];
};

#ifdef PROJECT_BUILD
int MsgDemoSend() {
#else
int main() {
#endif
    //建立消息队列
    int msgid = -1;
    msgid = msgget((key_t) 1234, 0666 | IPC_CREAT);
    if (msgid == -1) {
        fprintf(stderr, "msgget failed with error: %d\n", errno);
        exit(EXIT_FAILURE);
    }

    //向消息队列中写消息，直到写入end
    int running = 1;
    struct msg_st data;
    char buffer[BUFSIZ];
    while (running) {
        //输入数据
        printf("Enter some text: ");
        fgets(buffer, BUFSIZ, stdin);
        data.msg_type = 1;    //注意2
        strcpy(data.text, buffer);
        //向队列发送数据
        if (msgsnd(msgid, (void *) &data, MAX_TEXT, 0) == -1) {
            fprintf(stderr, "msgsnd failed\n");
            exit(EXIT_FAILURE);
        }
        //输入end结束输入
        if (strncmp(buffer, "end", 3) == 0) {
            running = 0;
        }
        std::this_thread::sleep_for(std::chrono::milliseconds(1000));
    }
    exit(EXIT_SUCCESS);
}
