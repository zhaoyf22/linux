//
// Created by zhaoyf on 2022/5/4.
//
#include <thread>
#include <chrono>

#include <stdio.h>

#include <sys/ipc.h>
#include <sys/shm.h>

#define BUF_SIZE 1024
#define SHM_KEY  0x19

#ifdef PROJECT_BUILD
int shmdemob() {
#else

int main() {
#endif
    int shmid = 0;
    char *shmptr = nullptr;

    if ((shmid = shmget(SHM_KEY, BUF_SIZE, IPC_CREAT)) == -1) {
        printf("shmget error!\n");
        return 1;
    }
    shmptr = (char *) shmat(shmid, nullptr, 0);
    if (shmptr == nullptr) {
        printf("shmat error!\n");
        return 1;
    }

    while (1) {
        printf("string: %s\n", shmptr);
        std::this_thread::sleep_for(std::chrono::milliseconds(2000));
    }
    return 0;
}

