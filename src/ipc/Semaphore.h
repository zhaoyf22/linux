//
// Created by zhaoyf on 2022/5/3.
//

#ifndef LINUX_SEMAPHORE_H
#define LINUX_SEMAPHORE_H

namespace Semaphore {
    /// 创建或者获取已存在的信号量
    /// \param num 要创建的信号量集中信号量数量， [0, 1, 2, ..., n-1]
    /// \param key 创建信号量的 key
    /// \return    信号量id
    int SemInit(int num, int key);

    /// p操作，信号量值减1
    /// \param semid 信号量id
    void SemP(int semid);

    /// v操作，信号量值加1
    /// \param semid 信号量id
    void SemV(int semid);

    /// 销毁信号量集中的所有信号量
    /// \param semid 信号量id
    /// \param num   信号量数量
    void SemRelease(int semid, int num);
}

#endif //LINUX_SEMAPHORE_H
